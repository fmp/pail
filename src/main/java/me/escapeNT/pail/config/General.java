package me.escapeNT.pail.config;

import javax.swing.UIManager;
import me.escapeNT.pail.Util.Util;

/**
 * Class for handling general configuration.
 *
 * @author escapeNT
 */
public final class General {
    /**
     * Loads the configuration.
     */
    public static void load() {
        defaults();
        Util.getPlugin().getConfig().options().copyDefaults(true);
    }

    private static void defaults() {
        Util.getPlugin().getConfig().addDefault("Autoupdate", true);
        Util.getPlugin().getConfig().addDefault("Skin", UIManager.getSystemLookAndFeelClassName());
        Util.getPlugin().getConfig().addDefault("SaveLogOnExit", false);
    }

    /**
     * Saves the configuration.
     */
    public static void save() {
        Util.getPlugin().getConfig().set("SaveLogOnExit", Util.getFileMenu().getSaveLog().isSelected());
        Util.getPlugin().saveConfig();
    }

    /**
     * @return the autoUpdate
     */
    public static boolean isAutoUpdate() {
        return Util.getPlugin().getConfig().getBoolean("Autoupdate");
    }

    public static boolean saveLogOnExit() {
        return Util.getPlugin().getConfig().getBoolean("SaveLogOnExit");
    }

    /**
     * @param aAutoUpdate the autoUpdate to set
     */
    public static void setAutoUpdate(boolean autoUpdate) {
        Util.getPlugin().getConfig().set("Autoupdate", autoUpdate);
    }

    /**
     * @return the lookAndFeel
     */
    public static String getLookAndFeel() {
        return Util.getPlugin().getConfig().getString("Skin");
    }

    /**
     * @param aLookAndFeel the lookAndFeel to set
     */
    public static void setLookAndFeel(String lookAndFeel) {
        Util.getPlugin().getConfig().set("Skin", lookAndFeel);
    }
}