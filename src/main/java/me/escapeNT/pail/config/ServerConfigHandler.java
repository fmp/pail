package me.escapeNT.pail.config;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import me.escapeNT.pail.Util.Util;
import org.bukkit.Bukkit;

/**
 * Class intended to handle the vanilla server configuration.
 *
 * @author escapeNT
 */
public final class ServerConfigHandler {
    static File file = new File("server.properties");

    /**
     * Saves the given configuration to file.
     *
     * @param properties The server properties.
     */
    public static void save(HashMap<String, String> properties) {
        if (newSave(properties)) {
            return;
        }

        if (!file.exists() || !file.canWrite()) {
            return;
        }

        file.delete();
        try {
            FileWriter fw = new FileWriter(file);
            PrintWriter writer = new PrintWriter(fw);

            writer.println("#Minecraft server properties");
            writer.println("#" + new Date(System.currentTimeMillis()).toString());

            for (String p : properties.keySet()) {
                writer.println(p + "=" + properties.get(p));
            }

            writer.close();
        } catch (IOException ex) {
            Util.log(Level.SEVERE, ex.toString());
        }
    }

    private static Object propertyManager;
    private static Method save;
    private static Method set;
    private static boolean attempted;
    private static boolean newSave(HashMap<String, String> properties) {
        if (propertyManager != null && save != null && set != null) {
            try {
                for (Map.Entry<String, String> entry : properties.entrySet()) {
                    set.invoke(propertyManager, entry.getKey(), entry.getValue());
                }
                save.invoke(propertyManager);
                return true;
            } catch (ReflectiveOperationException ex) {
            }
            return false;
        }

        if (attempted) {
            return false;
        }

        attempted = true;

        try {
            Field fConsole = Bukkit.getServer().getClass().getDeclaredField("console");
            fConsole.setAccessible(true);
            Object console = fConsole.get(Bukkit.getServer());
            propertyManager = console.getClass().getDeclaredMethod("getPropertyManager").invoke(console);
            try {
                // This is optional...
                file = (File) propertyManager.getClass().getDeclaredMethod("c").invoke(propertyManager);
            } catch(ReflectiveOperationException ex) {
            }
            save = propertyManager.getClass().getDeclaredMethod("savePropertiesFile");
            set = propertyManager.getClass().getDeclaredMethod("a", String.class, Object.class);
            return newSave(properties);
        } catch (ReflectiveOperationException ex) {
        } catch (SecurityException ex) {
        }

        return false;
    }
}